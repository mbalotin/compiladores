
%%

%{
  private int comment_count = 0;

  private AsdrGram yyparser;

  public Yylex(java.io.Reader r, AsdrGram yyparser) {
    this(r);
    this.yyparser = yyparser;
  }


%} 

%integer
%line
%char

WHITE_SPACE_CHAR=[\n\r\ \t\b\012]

%% 

[0-9]+(\.[0-9]+)? 	{ return AsdrGram.NUM; }
[a-Z]+(\.[0-9]+)? 	{ return AsdrGram.IDENT; }


int	{ return AsdrGram.INT; }
double	{ return AsdrGram.DOUBLE; }
boolean	{ return AsdrGram.BOOLEAN; }

if	{ return AsdrGram.IF; }
else	{ return AsdrGram.ELSE; }
while	{ return AsdrGram.WHILE; }


"(" |
")" |
"/" |
"%" |
"*" |
"-" |
"=" |
";" |
"{" |
"}" |
"," |
"+"    { return yytext().charAt(0); } 


{WHITE_SPACE_CHAR}+ { }

. { System.out.println("Erro lexico: caracter invalido: <" + yytext() + ">"); }






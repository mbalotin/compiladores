import java.io.*;

public class AsdrGram {

  private static final int BASE_TOKEN_NUM = 301;
  



  public static final int IDENT		= 301;
  public static final int NUM  		= 302;
  

  public static final int INT  		= 303;
  public static final int BOOLEAN  	= 304;
  public static final int DOUBLE  	= 305;


  public static final int IF  		= 306;
  public static final int ELSE  	= 307;
  public static final int WHILE  	= 308;

  public static final String tokenList[] = {"STRING", "NUM" };

/*****  Gramática
    Prog --> Decl  Bloco 
     
    Decl --> Tipo LId ';'  Decl 
            |     //produção vazia 
             
    Tipo --> int | double |boolean 
     
  LId --> IDENT LId2
  
  LId2 --> ',' IDENT LId 
          | 	//vazio 
              
  Bloco -->  '{' Decl LCmd '}' 
   
  LCmd -->  Cmd  LCmd 
          |     //produção vazia 
           
  Cmd --> Bloco 
        | if E Cmd ELSE
        | while E Cmd 
        | IDENT '=' E ';' 

  ELSE --> else Cmd
        |   	// vazio
        
  E --> T R

  R --> '+' T R
       | '-' T R
	   | 		//vazio

  T --> F S

  S --> '*' F S
       | 		// vazio

  F --> NUM 
      | IDENT 
      | '(' E ')' 
   

****/
                                      
  /* referencia ao objeto Scanner gerado pelo JFLEX */
  private Yylex lexer;

  public ParserVal yylval;

  private static int la;
  private boolean debug = true;
  
  /* construtor da classe */
  public AsdrGram (Reader r) {
      lexer = new Yylex (r, this);
  }
  
  public void setDebug(boolean dbg) {
      debug = dbg;
  }

  public void Prog() {
	if (la == IDENT)	Decl();
	Bloco();
  }

  private void Decl(){
	if (la == INT || la == DOUBLE || la == BOOLEAN){
  		Tipo();
		LId();
		check(';');
		Decl();
	}
  }

  private void Tipo(){
  	if (la == INT) check(INT);
  	else if (la == DOUBLE) check(DOUBLE);
  	else if (la == BOOLEAN) check(BOOLEAN);
	else yyerror("esperado INT, BOOLEAN, ou DOUBLE");
  }

  private void LId(){
  	if (la == IDENT){
		check(IDENT);
		Lid2();
  	} else yyerror("esperado IDENT");
  }

  private void LId2(){
  	if (la == ','){
		check(',');
		check(IDENT);
		LId();	
	}
  }

  private void Bloco(){
  	if (la == '{'){
		check('{');
		Decl();
		LCmd();
		check('}');
	}
	yyerror("esperado }");
  }

  private void LCmd(){
  	if (la == '{' || la == IF || la == WHILE || la == IDENT){
		Cmd();
		Bloco();
	}
  }


//TODO terminar analisador lexico
  private void Cmd(){
  	if (la == '{' ){
		Cmd();
		Bloco();
	}
  }

  private void check(int expected) {
      if (la == expected)
         la = this.yylex();
      else {
         // erro: esperado token "expected" veio token "la" 
         // System.out.printf("Erro: esperado %d, veio %d\n", expected, la)
         String expStr, laStr;       

		expStr = ((expected < BASE_TOKEN_NUM )
               ? ""+(char)expected
			         : tokenList[expected-BASE_TOKEN_NUM]);
         
		laStr = ((la < BASE_TOKEN_NUM )
                ? new Character((char)la).toString()
                : tokenList[la-BASE_TOKEN_NUM]);

          yyerror( "esperado token : " + expStr +
                   " na entrada: " + laStr);
     }
   }

   /* metodo de acesso ao Scanner gerado pelo JFLEX */
   private int yylex() {
       int retVal = -1;
       try {
           yylval = new ParserVal(0); //zera o valor do token
           retVal = lexer.yylex(); //le a entrada do arquivo e retorna um token
       } catch (IOException e) {
           System.err.println("IO Error:" + e);
          }
       return retVal; //retorna o token para o Parser 
   }

  /* metodo de manipulacao de erros de sintaxe */
  private void yyerror (String error) {
     System.err.println("Erro: " + error);
     System.err.println("Entrada rejeitada");
     System.out.println("\n\nFalhou!!!");
     System.exit(1);
     
  }

  /**
   * Runs the scanner on input files.
   *
   * This main method is the debugging routine for the scanner.
   * It prints debugging information about each returned token to
   * System.out until the end of file is reached, or an error occured.
   *
   * @param args   the command line, contains the filenames to run
   *               the scanner on.
   */
  public static void main(String[] args) {
     AsdrGram parser = null;
     try {
         if (args.length == 0)
            parser = new AsdrGram(new InputStreamReader(System.in));
         else 
            parser = new  AsdrGram( new java.io.FileReader(args[0]));


          la = parser.yylex();          

          parser.Prog();
     
          if (la== Yylex.YYEOF)
             System.out.println("\n\nSucesso!");
          else     
             System.out.println("\n\nFalhou - esperado EOF.");               

        }
        catch (java.io.FileNotFoundException e) {
          System.out.println("File not found : \""+args[0]+"\"");
        }
        catch (Exception e) {
          System.out.println("Unexpected exception:");
          e.printStackTrace();
      }
    
  }
  
}


/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * Copyright (C) 2001 Gerwin Klein <lsf@jflex.de>                          *
 * All rights reserved.                                                    *
 *                                                                         *
 * This is a modified version of the example from                          *
 *   http://www.lincom-asg.com/~rjamison/byacc/                            *
 *                                                                         *
 * Thanks to Larry Bell and Bob Jamison for suggestions and comments.      *
 *                                                                         *
 * This program is free software; you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License. See the file      *
 * COPYRIGHT for more information.                                         *
 *                                                                         *
 * This program is distributed in the hope that it will be useful,         *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 * GNU General Public License for more details.                            *
 *                                                                         *
 * You should have received a copy of the GNU General Public License along *
 * with this program; if not, write to the Free Software Foundation, Inc., *
 * 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA                 *
 *                                                                         *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

// Márcio Balotin (15280212-0) e Renata Bueno (14105009-6)

%{
  import java.io.*;
%}


%token 	CLASS
%token 	PRIVATE
%token 	PUBLIC

%token 	INT
%token 	DOUBLE
%token 	BOOLEAN_TYPE
%token 	STRING_TYPE
%token 	VOID
%token 	MAIN
%token 	BREAK
%token 	NEW

%token 	TRUE
%token 	FALSE

%token IF
%token ELSE
%token END_IF
%token FOR
%token END_FOR
%token WHILE
%token END_WHILE
%token ESCREVA
%token LEIA
%token RETURN

%token AND
%token OR
%token EQUALS
%token LOWER_EQUALS
%token GREATER_EQUALS
%token PLUS_EQUAL
%token MINUS_EQUAL
%token PLUS_PLUS
%token MINUS_MINUS
 
%token <dval> NUM
%token <sval> STRING
%token <sval> IDENT

%left '>' '<' LOWER_EQUALS GREATER_EQUALS
%left '-' '+'
%left '/' '*'
%left AND OR EQUALS DIFF
%right '!'
%right '^' '=' '%'


      
%%

s: CLASS IDENT '{' BLC_PRIVATE BLC_PUBLIC '}'
	;

BLC_PRIVATE: PRIVATE ':' DECL
	|
	;

DECL: TIPO IDENT ';' {ts.tabela.add(new RegistroTS( $2 , "", null ));} DECL
	|
	;

TIPO: INT
	| DOUBLE
	| STRING_TYPE
	| BOOLEAN_TYPE
	| IDENT
	;

BLC_PUBLIC: PUBLIC ':' BLC_PUBLIC2 
	;

BLC_PUBLIC2:  VOID METODO LIST_METODOS
	| INT METODO LIST_METODOS
	| DOUBLE METODO LIST_METODOS
	| STRING_TYPE METODO LIST_METODOS
	| BOOLEAN_TYPE METODO LIST_METODOS
	| IDENT BLC_PUBLIC3
	|
	;

BLC_PUBLIC3: '(' LIST_PARAM ')' DECL '{' LIST_COMANDOS '}' BLC_PUBLIC2
	| IDENT '(' LIST_PARAM ')' DECL '{' LIST_COMANDOS '}' LIST_METODOS
	;

LIST_METODOS: TIPO_RET METODO LIST_METODOS
	|
	;

METODO: IDENT '(' LIST_PARAM ')' DECL '{' LIST_COMANDOS '}'
	;

LIST_PARAM: TIPO IDENT LIST_PARAM2
	|
	;

LIST_PARAM2: ',' TIPO IDENT LIST_PARAM2
	|
	;

TIPO_RET: TIPO
	| VOID
	;

LIST_COMANDOS: COMANDO LIST_COMANDOS
	|
	;

COMANDO: ATRIBUICAO
	| INCREMENTO
	| ESCREVA_MET
	| LEIA_MET
	| IF_MET
	| FOR_MET
	| WHILE_MET
	| EXP ';'
	| BREAK ';'
	| RETURN  EXP ';'
	;

ATRIBUICAO: IDENT ATRIBUICAO2 ';'
	;

ATRIBUICAO2: '=' ATRIBUICAO3
	| PLUS_EQUAL EXP
	| MINUS_EQUAL EXP
	;

ATRIBUICAO3: EXP
	| NEW  IDENT '(' LIST_EXPES ')'
	;

INCREMENTO: IDENT INCREMENTO2 ';'
	;

INCREMENTO2: PLUS_PLUS
	| MINUS_MINUS
	;


BOOLEAN: TRUE
	| FALSE
	;

LIST_EXPES: EXP LIST_EXPES2
	|
	;

LIST_EXPES2: ',' EXP LIST_EXPES2 
	|
	;

LEIA_MET: LEIA IDENT LEIA_MET2 ';'
	;

LEIA_MET2: '.' IDENT LEIA_MET2
	| '(' LIST_EXPES ')' '.' IDENT LEIA_MET2
	| 
	;
	
ESCREVA_MET: ESCREVA EXP ESCREVA_MET2 ';'
	;

ESCREVA_MET2: ',' EXP 
	|
	;

IF_MET: IF EXP ':' LIST_COMANDOS IFELSE END_IF
	;

IFELSE: ELSE ':' LIST_COMANDOS
	|
	;

WHILE_MET: WHILE EXP ':' LIST_COMANDOS END_WHILE
	;

FOR_MET: FOR ATRIBUICAO EXP ';' INCREMENTO_FOR ':' LIST_COMANDOS END_FOR
	;

INCREMENTO_FOR: IDENT INCREMENTO_FOR2
	;

INCREMENTO_FOR2: '=' EXP
	| PLUS_EQUAL EXP
	| MINUS_EQUAL EXP
	| PLUS_PLUS
	| MINUS_MINUS
	;

EXP:  EXP_2             
       | EXP '+' EXP
       | EXP '-' EXP
       | EXP '*' EXP
       | EXP '/' EXP
       | EXP '^' EXP
       | EXP '%' EXP
       | EXP '<' EXP
       | EXP '>' EXP
       | EXP LOWER_EQUALS EXP
       | EXP GREATER_EQUALS EXP
       | EXP AND EXP
       | EXP OR EXP
       | EXP EQUALS EXP
       | EXP DIFF EXP
	   | '!' EXP
       ;

EXP_2: IDENT EXP_3
	| NUM 
	| STRING
	| BOOLEAN
	| '(' EXP ')'
	;

EXP_3: '.' IDENT EXP_3
	| '(' LIST_EXPES ')' EXP_3
	| 
	;

%%

  private AJavaLex lexer;

  private TabelaSimbolos ts;

  private int yylex () {
    int yyl_return = -1;
    try {
      yylval = new ParserVal(0);
      yyl_return = lexer.yylex();
    }
    catch (IOException e) {
      System.err.println("IO error :"+e);
    }

    // System.err.println(lexer.yytext());

    return yyl_return;
  }


  public void yyerror (String error) {
    System.err.println("Error : "+lexer.getLine()+ "msg: " + error);
  }


  public Parser(Reader r) {
    lexer = new AJavaLex(r, this);
    ts = new TabelaSimbolos();
  }


  static boolean interactive;

  public static void main(String args[]) throws IOException {
    System.out.println("aJava Parser");

    Parser yyparser;
    if ( args.length > 0 ) {
      	// parse a file
      	yyparser = new Parser(new FileReader(args[0]));
    }
    else {
      	// interactive mode
      	System.out.println("[Quit with CTRL-D]");
      	System.out.print("Expression: ");
      	interactive = true;
		yyparser = new Parser(new InputStreamReader(System.in));
    }

    yyparser.yyparse();




    yyparser.ts.print();
    
  }

//### This file created by BYACC 1.8(/Java extension  1.15)
//### Java capabilities added 7 Jan 97, Bob Jamison
//### Updated : 27 Nov 97  -- Bob Jamison, Joe Nieten
//###           01 Jan 98  -- Bob Jamison -- fixed generic semantic constructor
//###           01 Jun 99  -- Bob Jamison -- added Runnable support
//###           06 Aug 00  -- Bob Jamison -- made state variables class-global
//###           03 Jan 01  -- Bob Jamison -- improved flags, tracing
//###           16 May 01  -- Bob Jamison -- added custom stack sizing
//###           04 Mar 02  -- Yuval Oren  -- improved java performance, added options
//###           14 Mar 02  -- Tomas Hurka -- -d support, static initializer workaround
//### Please send bug reports to tom@hukatronic.cz
//### static char yysccsid[] = "@(#)yaccpar	1.8 (Berkeley) 01/20/90";






//#line 28 "aJava.y"
  import java.io.*;
//#line 19 "Parser.java"




public class Parser
{

boolean yydebug;        //do I want debug output?
int yynerrs;            //number of errors so far
int yyerrflag;          //was there an error?
int yychar;             //the current working character

//########## MESSAGES ##########
//###############################################################
// method: debug
//###############################################################
void debug(String msg)
{
  if (yydebug)
    System.out.println(msg);
}

//########## STATE STACK ##########
final static int YYSTACKSIZE = 500;  //maximum stack size
int statestk[] = new int[YYSTACKSIZE]; //state stack
int stateptr;
int stateptrmax;                     //highest index of stackptr
int statemax;                        //state when highest index reached
//###############################################################
// methods: state stack push,pop,drop,peek
//###############################################################
final void state_push(int state)
{
  try {
		stateptr++;
		statestk[stateptr]=state;
	 }
	 catch (ArrayIndexOutOfBoundsException e) {
     int oldsize = statestk.length;
     int newsize = oldsize * 2;
     int[] newstack = new int[newsize];
     System.arraycopy(statestk,0,newstack,0,oldsize);
     statestk = newstack;
     statestk[stateptr]=state;
  }
}
final int state_pop()
{
  return statestk[stateptr--];
}
final void state_drop(int cnt)
{
  stateptr -= cnt; 
}
final int state_peek(int relative)
{
  return statestk[stateptr-relative];
}
//###############################################################
// method: init_stacks : allocate and prepare stacks
//###############################################################
final boolean init_stacks()
{
  stateptr = -1;
  val_init();
  return true;
}
//###############################################################
// method: dump_stacks : show n levels of the stacks
//###############################################################
void dump_stacks(int count)
{
int i;
  System.out.println("=index==state====value=     s:"+stateptr+"  v:"+valptr);
  for (i=0;i<count;i++)
    System.out.println(" "+i+"    "+statestk[i]+"      "+valstk[i]);
  System.out.println("======================");
}


//########## SEMANTIC VALUES ##########
//public class ParserVal is defined in ParserVal.java


String   yytext;//user variable to return contextual strings
ParserVal yyval; //used to return semantic vals from action routines
ParserVal yylval;//the 'lval' (result) I got from yylex()
ParserVal valstk[];
int valptr;
//###############################################################
// methods: value stack push,pop,drop,peek.
//###############################################################
void val_init()
{
  valstk=new ParserVal[YYSTACKSIZE];
  yyval=new ParserVal();
  yylval=new ParserVal();
  valptr=-1;
}
void val_push(ParserVal val)
{
  if (valptr>=YYSTACKSIZE)
    return;
  valstk[++valptr]=val;
}
ParserVal val_pop()
{
  if (valptr<0)
    return new ParserVal();
  return valstk[valptr--];
}
void val_drop(int cnt)
{
int ptr;
  ptr=valptr-cnt;
  if (ptr<0)
    return;
  valptr = ptr;
}
ParserVal val_peek(int relative)
{
int ptr;
  ptr=valptr-relative;
  if (ptr<0)
    return new ParserVal();
  return valstk[ptr];
}
final ParserVal dup_yyval(ParserVal val)
{
  ParserVal dup = new ParserVal();
  dup.ival = val.ival;
  dup.dval = val.dval;
  dup.sval = val.sval;
  dup.obj = val.obj;
  return dup;
}
//#### end semantic value section ####
public final static short CLASS=257;
public final static short PRIVATE=258;
public final static short PUBLIC=259;
public final static short INT=260;
public final static short DOUBLE=261;
public final static short BOOLEAN_TYPE=262;
public final static short STRING_TYPE=263;
public final static short VOID=264;
public final static short MAIN=265;
public final static short BREAK=266;
public final static short NEW=267;
public final static short TRUE=268;
public final static short FALSE=269;
public final static short IF=270;
public final static short ELSE=271;
public final static short END_IF=272;
public final static short FOR=273;
public final static short END_FOR=274;
public final static short WHILE=275;
public final static short END_WHILE=276;
public final static short ESCREVA=277;
public final static short LEIA=278;
public final static short RETURN=279;
public final static short AND=280;
public final static short OR=281;
public final static short EQUALS=282;
public final static short LOWER_EQUALS=283;
public final static short GREATER_EQUALS=284;
public final static short PLUS_EQUAL=285;
public final static short MINUS_EQUAL=286;
public final static short PLUS_PLUS=287;
public final static short MINUS_MINUS=288;
public final static short NUM=289;
public final static short STRING=290;
public final static short IDENT=291;
public final static short DIFF=292;
public final static short YYERRCODE=256;
final static short yylhs[] = {                           -1,
    0,    1,    1,    5,    3,    3,    4,    4,    4,    4,
    4,    2,    6,    6,    6,    6,    6,    6,    6,    9,
    9,    8,    8,    7,   10,   10,   13,   13,   12,   12,
   11,   11,   14,   14,   14,   14,   14,   14,   14,   14,
   14,   14,   15,   23,   23,   23,   24,   24,   16,   26,
   26,   27,   27,   25,   25,   28,   28,   18,   29,   29,
   29,   17,   30,   30,   19,   31,   31,   21,   20,   32,
   33,   33,   33,   33,   33,   22,   22,   22,   22,   22,
   22,   22,   22,   22,   22,   22,   22,   22,   22,   22,
   22,   34,   34,   34,   34,   34,   35,   35,   35,
};
final static short yylen[] = {                            2,
    6,    3,    0,    0,    5,    0,    1,    1,    1,    1,
    1,    3,    3,    3,    3,    3,    3,    2,    0,    8,
    9,    3,    0,    8,    3,    0,    4,    0,    1,    1,
    2,    0,    1,    1,    1,    1,    1,    1,    1,    2,
    2,    3,    3,    2,    2,    2,    1,    5,    3,    1,
    1,    1,    1,    2,    0,    3,    0,    4,    3,    6,
    0,    4,    2,    0,    6,    3,    0,    5,    8,    2,
    2,    2,    2,    1,    1,    1,    3,    3,    3,    3,
    3,    3,    3,    3,    3,    3,    3,    3,    3,    3,
    2,    2,    1,    1,    1,    3,    3,    4,    0,
};
final static short yydefred[] = {                         0,
    0,    0,    0,    0,    0,    0,    0,    0,    0,    7,
    8,   10,    9,   11,    2,    0,    0,    1,    0,    0,
    0,    0,    0,    0,    0,   12,    4,    0,    0,    0,
    0,    0,    0,    0,    0,   18,    0,    0,   30,   29,
   14,    0,   15,   17,   16,   13,    0,    0,    0,    5,
    0,    0,    0,    0,    0,    0,   22,    0,    0,   25,
    0,    0,    0,    0,    0,    0,    0,    0,    0,   52,
   53,    0,    0,    0,    0,    0,    0,   93,   94,    0,
    0,    0,    0,    0,   33,   34,   35,   36,   37,   38,
   39,    0,   95,   76,    0,    0,   27,   41,    0,    0,
    0,    0,    0,    0,    0,    0,    0,    0,   50,   51,
    0,    0,    0,    0,    0,   92,    0,    0,    0,   31,
    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,
    0,    0,    0,    0,   40,   24,    0,    0,    0,    0,
    0,    0,    0,    0,    0,   42,    0,    0,    0,    0,
   44,    0,    0,    0,   43,   49,   96,   20,    0,    0,
    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,
    0,    0,   21,    0,    0,    0,    0,   62,    0,    0,
   58,    0,    0,   54,    0,   97,    0,    0,    0,    0,
   68,    0,   59,    0,    0,   98,    0,   65,    0,    0,
   74,   75,    0,   70,    0,    0,    0,   56,   66,    0,
    0,    0,    0,    0,   48,   69,   60,
};
final static short yydgoto[] = {                          2,
    6,    9,   15,   40,   37,   26,   29,   41,   36,   49,
   83,   42,   60,   84,   85,   86,   87,   88,   89,   90,
   91,   92,  114,  151,  153,  115,   93,  184,  145,  142,
  188,  190,  204,   94,  116,
};
final static short yysindex[] = {                      -248,
 -277,    0, -101, -230,  -11, -221, -245,   -4,  -63,    0,
    0,    0,    0,    0,    0, -203, -228,    0,   31, -196,
 -196, -196, -196, -196,  -39,    0,    0,   58,   52,   52,
   52,   52,   52,   66, -245,    0, -245, -245,    0,    0,
    0, -196,    0,    0,    0,    0, -245, -187,   69,    0,
   76,   52,   81,   75, -245, -245,    0, -245, -245,    0,
   12,   25,   36, -179,  -33,  -33,  -33,   75,   70,    0,
    0,  -30, -141,  -30,  -30, -131,  -30,    0,    0,   -9,
  -30,  -30,   38,  -33,    0,    0,    0,    0,    0,    0,
    0,    6,    0,    0,   41,   42,    0,    0,   21,   13,
  -31,  -30,   27,   49,   57,   71,  -30,  -30,    0,    0,
  -20,  -30, -121,  118,  120,    0,  -35,   95, -228,    0,
  -30,  -30,  -30,  -30,  -30,  -30,  -30,  -30,  -30,  -30,
  -30,  -30,  -30,  -30,    0,    0,   52,  -33,  102,  -33,
  -30,  123,  -30, -105,  128,    0,  138,  138, -103,  138,
    0,  109,  151,   21,    0,    0,    0,    0,  -35,  -35,
  -35,  173,  173,  173,  173,   78,   78,   64,   64,  -35,
  -35,  -35,    0,  -77,  -96,  -79,  138,    0,  158,   57,
    0,  161,  -30,    0,   21,    0,  144,  -68,  -57,  154,
    0,  167,    0,  -30,  109,    0,  -33,    0,  -30,  -30,
    0,    0,  -30,    0,  -33,  -74,  178,    0,    0,  138,
  138,  138,  -60,   57,    0,    0,    0,
};
final static short yyrindex[] = {                         0,
    0,    0,    0,  -38,    0,    0,  -32,    0,    0,    0,
    0,    0,    0,    0,    0,    0,  116,    0,    0,    0,
    0,    0,    0,    0,    0,    0,    0,    0,  125,  125,
  125,  125,  125,    0,  193,    0, -118,  193,    0,    0,
    0,    0,    0,    0,    0,    0,  193,    0,    0,    0,
    0,  125,    0,  202,  130,  130,    0,  130,    0,    0,
    0,    0,    0,    0,  126,  126,  126,  202,    0,    0,
    0,    0,    0,    0,    0,    0,    0,    0,    0,  131,
    0,    0,    0,    9,    0,    0,    0,    0,    0,    0,
    0,    0,    0,    0,    0,    0,    0,    0,  -18,    0,
    0,    0,    0,  209,  213,    0,    0,    0,    0,    0,
    0,   69,    0,    0,    0,    0,  164,    0,  116,    0,
    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,
    0,    0,    0,    0,    0,    0,  125, -144,    0,   -3,
    0,    0,   69,    0,    0,    0,  216,  223,    0,  225,
    0,  250,    0,  -18,    0,    0,    0,    0,  280,  416,
  438,  366,  408,  487,  537,  532,  544,  496,  524,  445,
  467,  474,    0,   20,    0,    0,  240,    0,    0,  213,
    0,    0,    0,    0,  -18,    0,    0,    0,    0,    0,
    0,    0,    0,   69,  250,    0,   30,    0,    0,    0,
    0,    0,    0,    0,   29,    0,    0,    0,    0,  246,
  248,  259,    0,  213,    0,    0,    0,
};
final static short yygindex[] = {                         0,
    0,    0,   68,  572,    0,  201,   60,   47,    0,  -26,
  234,    0,  258,    0,  255,    0,    0,    0,    0,    0,
    0,  540,    0,    0, -137,    0,    0,  139, -169,    0,
    0,    0,    0,    0, -146,
};
final static int YYTABLESIZE=828;
static short yytable[];
static { yytable();}
static void yytable(){
yytable = new short[]{                         81,
   35,  134,   81,  203,    6,  179,   82,  186,    1,   82,
  193,   51,   81,    3,   10,   11,   12,   13,   99,   82,
   53,    4,   99,   99,   99,   99,   99,    5,   99,  111,
  112,   20,   21,   22,   23,   24,  113,    8,  196,   99,
   99,   99,  134,   99,  217,   14,    7,  131,  129,  134,
  128,  111,  130,   17,  131,  129,  207,  128,  133,  130,
  112,   18,   25,  134,  135,  127,  113,  126,  131,  129,
  138,  128,  127,  130,  126,   99,   43,   44,   45,   46,
   30,   31,   32,   33,  140,  134,  127,   19,  126,   27,
  131,  129,  141,  128,   28,  130,  143,   38,   57,  133,
  134,   52,  144,   54,   50,   47,  133,  134,  127,   55,
  126,   68,  131,  129,  134,  128,   56,  130,   59,  131,
  133,   58,   61,   62,  130,   63,   32,   32,   98,  146,
  127,  134,  126,   32,   65,  157,  131,  129,  134,  128,
    6,  130,  133,  131,  129,  134,  128,   66,  130,  101,
  131,  129,  183,  128,  127,  130,  126,  133,   67,  105,
  175,  127,  119,  126,  133,  136,  137,   99,  127,  154,
  126,  133,   99,   99,  134,   99,  155,   99,  156,  131,
  129,  178,  128,  173,  130,  180,  181,  182,  133,   99,
   99,  185,   99,  187,  189,  133,  191,  127,  192,  126,
  194,  197,  133,  198,   91,   91,   91,   91,   91,  134,
   91,  205,  206,  216,  131,  129,  214,  128,  215,  130,
    3,   91,   91,   91,   99,   91,    6,  199,  200,  201,
  202,  133,   69,   26,   70,   71,   72,   70,   71,   73,
   19,   74,   28,   75,   76,   77,  149,   70,   71,   23,
   32,   34,    6,  107,  108,   78,   79,   80,   78,   79,
   99,   99,   99,   99,   99,   99,  133,   64,   78,   79,
   99,   61,   32,   99,   45,  107,  108,  109,  110,   32,
   32,   46,   32,   47,   32,  121,  122,  123,  124,  125,
   57,   67,  121,  122,  123,  124,  125,  132,   63,   95,
   96,   32,   32,   72,  132,   73,  121,  122,  123,  124,
  125,   10,   11,   12,   13,   39,   71,  120,  132,  158,
   87,   87,   87,   87,   87,   97,   87,  102,  121,  122,
  123,  124,  125,  208,    0,    0,    0,   87,   87,   87,
  132,   87,   14,  121,  122,  123,    0,    0,    0,    0,
  121,  122,  123,  124,  125,  132,    0,  121,  122,  123,
    0,    0,  132,    0,    0,    0,    0,    0,    0,  132,
    0,  174,    0,  176,  121,  122,  123,  124,  125,    0,
    0,  121,  122,  123,  124,  125,  132,    0,  121,  122,
  123,  124,  125,  132,    0,    0,    0,    0,    0,    0,
  132,    0,    0,    0,    0,    0,   85,    0,    0,   85,
   99,   99,   99,   99,   99,    0,    0,  121,  122,  123,
  124,  125,   99,   85,   85,   85,    0,   85,    0,  132,
  209,    0,    0,    0,    0,    0,    0,    0,  213,    0,
    0,    0,    0,   91,   91,   91,   91,   91,   86,    0,
    0,   86,  121,  122,  123,   91,   88,   88,   88,   88,
   88,    0,   88,    0,  132,   86,   86,   86,    0,   86,
    0,    0,    0,   88,   88,   88,    0,   88,   89,   89,
   89,   89,   89,    0,   89,   90,   90,   90,   90,   90,
    0,   90,    0,    0,    0,   89,   89,   89,    0,   89,
    0,    0,   90,   90,   90,    0,   90,   81,   81,   81,
   81,   81,    0,   81,   82,   82,   82,   82,   82,    0,
   82,    0,    0,    0,   81,   81,   81,   84,   81,    0,
   84,   82,   82,   82,    0,   82,   80,   80,   80,   80,
   80,    0,   80,    0,   84,   84,   84,    0,   84,    0,
    0,    0,    0,   80,   80,   80,    0,   80,    0,   87,
   87,   87,   87,   87,   79,   79,   79,   79,   79,    0,
   79,   87,   78,    0,   78,   78,   78,   83,   16,    0,
   83,   79,   79,   79,   77,   79,   77,   77,   77,   78,
   78,   78,    0,   78,   83,   83,   83,    0,   83,    0,
    0,   77,   77,   77,    0,   77,   48,    0,   16,   48,
    0,  100,    0,  103,  104,    0,  106,    0,   48,    0,
  117,  118,    0,    0,    0,    0,   16,   16,    0,   16,
   64,    0,    0,    0,    0,    0,    0,    0,    0,    0,
    0,  139,    0,    0,    0,    0,  147,  148,   85,   85,
  150,  152,    0,    0,    0,    0,    0,    0,    0,    0,
  159,  160,  161,  162,  163,  164,  165,  166,  167,  168,
  169,  170,  171,  172,    0,    0,    0,    0,    0,    0,
  177,    0,  152,    0,    0,    0,    0,    0,    0,    0,
   86,   86,    0,    0,    0,   88,   88,   88,   88,   88,
    0,    0,    0,    0,    0,    0,    0,   88,    0,    0,
    0,    0,    0,    0,    0,    0,    0,   89,   89,   89,
   89,   89,  195,    0,   90,   90,   90,   90,   90,   89,
    0,    0,    0,  152,    0,    0,   90,    0,  210,  211,
    0,    0,  212,    0,    0,    0,   81,   81,   81,   81,
   81,    0,    0,   82,   82,   82,   82,   82,   81,    0,
    0,    0,    0,    0,    0,   82,    0,    0,    0,   84,
   84,    0,    0,    0,    0,    0,    0,    0,   80,   80,
    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,
    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,
    0,    0,    0,    0,    0,    0,   79,   79,    0,    0,
    0,    0,    0,    0,   78,   78,    0,    0,    0,   83,
   83,    0,    0,    0,    0,    0,   77,   77,
};
}
static short yycheck[];
static { yycheck(); }
static void yycheck() {
yycheck = new short[] {                         33,
   40,   37,   33,   61,  123,  143,   40,  154,  257,   40,
  180,   38,   33,  291,  260,  261,  262,  263,   37,   40,
   47,  123,   41,   42,   43,   44,   45,  258,   47,   61,
   40,  260,  261,  262,  263,  264,   46,  259,  185,   58,
   59,   60,   37,   62,  214,  291,   58,   42,   43,   37,
   45,   61,   47,   58,   42,   43,  194,   45,   94,   47,
   40,  125,  291,   37,   59,   60,   46,   62,   42,   43,
   58,   45,   60,   47,   62,   94,   30,   31,   32,   33,
   21,   22,   23,   24,   58,   37,   60,  291,   62,   59,
   42,   43,   44,   45,  291,   47,   40,   40,   52,   94,
   37,   42,   46,  291,   37,   40,   94,   37,   60,   41,
   62,  291,   42,   43,   37,   45,   41,   47,   44,   42,
   94,   41,   55,   56,   47,   58,  271,  272,   59,   59,
   60,   37,   62,  125,  123,   41,   42,   43,   37,   45,
  259,   47,   94,   42,   43,   37,   45,  123,   47,  291,
   42,   43,   44,   45,   60,   47,   62,   94,  123,  291,
   59,   60,  125,   62,   94,  125,  125,   37,   60,  291,
   62,   94,   42,   43,   37,   45,   59,   47,   59,   42,
   43,   59,   45,  137,   47,  291,   59,  291,   94,   59,
   60,   41,   62,  271,  291,   94,  276,   60,   41,   62,
   40,   58,   94,  272,   41,   42,   43,   44,   45,   37,
   47,   58,   46,  274,   42,   43,  291,   45,   41,   47,
  259,   58,   59,   60,   94,   62,  259,  285,  286,  287,
  288,   94,  266,   41,  268,  269,  270,  268,  269,  273,
  125,  275,   41,  277,  278,  279,  267,  268,  269,  125,
  125,  291,  123,  285,  286,  289,  290,  291,  289,  290,
  291,  280,  281,  282,  283,  284,   94,   59,  289,  290,
  291,   59,  276,  292,   59,  285,  286,  287,  288,  271,
  272,   59,  274,   59,  276,  280,  281,  282,  283,  284,
   41,  272,  280,  281,  282,  283,  284,  292,   59,   66,
   67,  272,  274,   58,  292,   58,  280,  281,  282,  283,
  284,  260,  261,  262,  263,  264,   58,   84,  292,  119,
   41,   42,   43,   44,   45,   68,   47,   73,  280,  281,
  282,  283,  284,  195,   -1,   -1,   -1,   58,   59,   60,
  292,   62,  291,  280,  281,  282,   -1,   -1,   -1,   -1,
  280,  281,  282,  283,  284,  292,   -1,  280,  281,  282,
   -1,   -1,  292,   -1,   -1,   -1,   -1,   -1,   -1,  292,
   -1,  138,   -1,  140,  280,  281,  282,  283,  284,   -1,
   -1,  280,  281,  282,  283,  284,  292,   -1,  280,  281,
  282,  283,  284,  292,   -1,   -1,   -1,   -1,   -1,   -1,
  292,   -1,   -1,   -1,   -1,   -1,   41,   -1,   -1,   44,
  280,  281,  282,  283,  284,   -1,   -1,  280,  281,  282,
  283,  284,  292,   58,   59,   60,   -1,   62,   -1,  292,
  197,   -1,   -1,   -1,   -1,   -1,   -1,   -1,  205,   -1,
   -1,   -1,   -1,  280,  281,  282,  283,  284,   41,   -1,
   -1,   44,  280,  281,  282,  292,   41,   42,   43,   44,
   45,   -1,   47,   -1,  292,   58,   59,   60,   -1,   62,
   -1,   -1,   -1,   58,   59,   60,   -1,   62,   41,   42,
   43,   44,   45,   -1,   47,   41,   42,   43,   44,   45,
   -1,   47,   -1,   -1,   -1,   58,   59,   60,   -1,   62,
   -1,   -1,   58,   59,   60,   -1,   62,   41,   42,   43,
   44,   45,   -1,   47,   41,   42,   43,   44,   45,   -1,
   47,   -1,   -1,   -1,   58,   59,   60,   41,   62,   -1,
   44,   58,   59,   60,   -1,   62,   41,   42,   43,   44,
   45,   -1,   47,   -1,   58,   59,   60,   -1,   62,   -1,
   -1,   -1,   -1,   58,   59,   60,   -1,   62,   -1,  280,
  281,  282,  283,  284,   41,   42,   43,   44,   45,   -1,
   47,  292,   41,   -1,   43,   44,   45,   41,    7,   -1,
   44,   58,   59,   60,   41,   62,   43,   44,   45,   58,
   59,   60,   -1,   62,   58,   59,   60,   -1,   62,   -1,
   -1,   58,   59,   60,   -1,   62,   35,   -1,   37,   38,
   -1,   72,   -1,   74,   75,   -1,   77,   -1,   47,   -1,
   81,   82,   -1,   -1,   -1,   -1,   55,   56,   -1,   58,
   59,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,
   -1,  102,   -1,   -1,   -1,   -1,  107,  108,  283,  284,
  111,  112,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,
  121,  122,  123,  124,  125,  126,  127,  128,  129,  130,
  131,  132,  133,  134,   -1,   -1,   -1,   -1,   -1,   -1,
  141,   -1,  143,   -1,   -1,   -1,   -1,   -1,   -1,   -1,
  283,  284,   -1,   -1,   -1,  280,  281,  282,  283,  284,
   -1,   -1,   -1,   -1,   -1,   -1,   -1,  292,   -1,   -1,
   -1,   -1,   -1,   -1,   -1,   -1,   -1,  280,  281,  282,
  283,  284,  183,   -1,  280,  281,  282,  283,  284,  292,
   -1,   -1,   -1,  194,   -1,   -1,  292,   -1,  199,  200,
   -1,   -1,  203,   -1,   -1,   -1,  280,  281,  282,  283,
  284,   -1,   -1,  280,  281,  282,  283,  284,  292,   -1,
   -1,   -1,   -1,   -1,   -1,  292,   -1,   -1,   -1,  283,
  284,   -1,   -1,   -1,   -1,   -1,   -1,   -1,  283,  284,
   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,
   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,   -1,
   -1,   -1,   -1,   -1,   -1,   -1,  283,  284,   -1,   -1,
   -1,   -1,   -1,   -1,  283,  284,   -1,   -1,   -1,  283,
  284,   -1,   -1,   -1,   -1,   -1,  283,  284,
};
}
final static short YYFINAL=2;
final static short YYMAXTOKEN=292;
final static String yyname[] = {
"end-of-file",null,null,null,null,null,null,null,null,null,null,null,null,null,
null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,
null,null,null,"'!'",null,null,null,"'%'",null,null,"'('","')'","'*'","'+'",
"','","'-'","'.'","'/'",null,null,null,null,null,null,null,null,null,null,"':'",
"';'","'<'","'='","'>'",null,null,null,null,null,null,null,null,null,null,null,
null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,
null,null,null,null,"'^'",null,null,null,null,null,null,null,null,null,null,
null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,
null,null,"'{'",null,"'}'",null,null,null,null,null,null,null,null,null,null,
null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,
null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,
null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,
null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,
null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,
null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,
null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,
null,null,null,null,null,null,null,null,null,"CLASS","PRIVATE","PUBLIC","INT",
"DOUBLE","BOOLEAN_TYPE","STRING_TYPE","VOID","MAIN","BREAK","NEW","TRUE",
"FALSE","IF","ELSE","END_IF","FOR","END_FOR","WHILE","END_WHILE","ESCREVA",
"LEIA","RETURN","AND","OR","EQUALS","LOWER_EQUALS","GREATER_EQUALS",
"PLUS_EQUAL","MINUS_EQUAL","PLUS_PLUS","MINUS_MINUS","NUM","STRING","IDENT",
"DIFF",
};
final static String yyrule[] = {
"$accept : s",
"s : CLASS IDENT '{' BLC_PRIVATE BLC_PUBLIC '}'",
"BLC_PRIVATE : PRIVATE ':' DECL",
"BLC_PRIVATE :",
"$$1 :",
"DECL : TIPO IDENT ';' $$1 DECL",
"DECL :",
"TIPO : INT",
"TIPO : DOUBLE",
"TIPO : STRING_TYPE",
"TIPO : BOOLEAN_TYPE",
"TIPO : IDENT",
"BLC_PUBLIC : PUBLIC ':' BLC_PUBLIC2",
"BLC_PUBLIC2 : VOID METODO LIST_METODOS",
"BLC_PUBLIC2 : INT METODO LIST_METODOS",
"BLC_PUBLIC2 : DOUBLE METODO LIST_METODOS",
"BLC_PUBLIC2 : STRING_TYPE METODO LIST_METODOS",
"BLC_PUBLIC2 : BOOLEAN_TYPE METODO LIST_METODOS",
"BLC_PUBLIC2 : IDENT BLC_PUBLIC3",
"BLC_PUBLIC2 :",
"BLC_PUBLIC3 : '(' LIST_PARAM ')' DECL '{' LIST_COMANDOS '}' BLC_PUBLIC2",
"BLC_PUBLIC3 : IDENT '(' LIST_PARAM ')' DECL '{' LIST_COMANDOS '}' LIST_METODOS",
"LIST_METODOS : TIPO_RET METODO LIST_METODOS",
"LIST_METODOS :",
"METODO : IDENT '(' LIST_PARAM ')' DECL '{' LIST_COMANDOS '}'",
"LIST_PARAM : TIPO IDENT LIST_PARAM2",
"LIST_PARAM :",
"LIST_PARAM2 : ',' TIPO IDENT LIST_PARAM2",
"LIST_PARAM2 :",
"TIPO_RET : TIPO",
"TIPO_RET : VOID",
"LIST_COMANDOS : COMANDO LIST_COMANDOS",
"LIST_COMANDOS :",
"COMANDO : ATRIBUICAO",
"COMANDO : INCREMENTO",
"COMANDO : ESCREVA_MET",
"COMANDO : LEIA_MET",
"COMANDO : IF_MET",
"COMANDO : FOR_MET",
"COMANDO : WHILE_MET",
"COMANDO : EXP ';'",
"COMANDO : BREAK ';'",
"COMANDO : RETURN EXP ';'",
"ATRIBUICAO : IDENT ATRIBUICAO2 ';'",
"ATRIBUICAO2 : '=' ATRIBUICAO3",
"ATRIBUICAO2 : PLUS_EQUAL EXP",
"ATRIBUICAO2 : MINUS_EQUAL EXP",
"ATRIBUICAO3 : EXP",
"ATRIBUICAO3 : NEW IDENT '(' LIST_EXPES ')'",
"INCREMENTO : IDENT INCREMENTO2 ';'",
"INCREMENTO2 : PLUS_PLUS",
"INCREMENTO2 : MINUS_MINUS",
"BOOLEAN : TRUE",
"BOOLEAN : FALSE",
"LIST_EXPES : EXP LIST_EXPES2",
"LIST_EXPES :",
"LIST_EXPES2 : ',' EXP LIST_EXPES2",
"LIST_EXPES2 :",
"LEIA_MET : LEIA IDENT LEIA_MET2 ';'",
"LEIA_MET2 : '.' IDENT LEIA_MET2",
"LEIA_MET2 : '(' LIST_EXPES ')' '.' IDENT LEIA_MET2",
"LEIA_MET2 :",
"ESCREVA_MET : ESCREVA EXP ESCREVA_MET2 ';'",
"ESCREVA_MET2 : ',' EXP",
"ESCREVA_MET2 :",
"IF_MET : IF EXP ':' LIST_COMANDOS IFELSE END_IF",
"IFELSE : ELSE ':' LIST_COMANDOS",
"IFELSE :",
"WHILE_MET : WHILE EXP ':' LIST_COMANDOS END_WHILE",
"FOR_MET : FOR ATRIBUICAO EXP ';' INCREMENTO_FOR ':' LIST_COMANDOS END_FOR",
"INCREMENTO_FOR : IDENT INCREMENTO_FOR2",
"INCREMENTO_FOR2 : '=' EXP",
"INCREMENTO_FOR2 : PLUS_EQUAL EXP",
"INCREMENTO_FOR2 : MINUS_EQUAL EXP",
"INCREMENTO_FOR2 : PLUS_PLUS",
"INCREMENTO_FOR2 : MINUS_MINUS",
"EXP : EXP_2",
"EXP : EXP '+' EXP",
"EXP : EXP '-' EXP",
"EXP : EXP '*' EXP",
"EXP : EXP '/' EXP",
"EXP : EXP '^' EXP",
"EXP : EXP '%' EXP",
"EXP : EXP '<' EXP",
"EXP : EXP '>' EXP",
"EXP : EXP LOWER_EQUALS EXP",
"EXP : EXP GREATER_EQUALS EXP",
"EXP : EXP AND EXP",
"EXP : EXP OR EXP",
"EXP : EXP EQUALS EXP",
"EXP : EXP DIFF EXP",
"EXP : '!' EXP",
"EXP_2 : IDENT EXP_3",
"EXP_2 : NUM",
"EXP_2 : STRING",
"EXP_2 : BOOLEAN",
"EXP_2 : '(' EXP ')'",
"EXP_3 : '.' IDENT EXP_3",
"EXP_3 : '(' LIST_EXPES ')' EXP_3",
"EXP_3 :",
};

//#line 254 "aJava.y"

  private AJavaLex lexer;

  private TabelaSimbolos ts;

  private int yylex () {
    int yyl_return = -1;
    try {
      yylval = new ParserVal(0);
      yyl_return = lexer.yylex();
    }
    catch (IOException e) {
      System.err.println("IO error :"+e);
    }

    // System.err.println(lexer.yytext());

    return yyl_return;
  }


  public void yyerror (String error) {
    System.err.println("Error : "+lexer.getLine()+ "msg: " + error);
  }


  public Parser(Reader r) {
    lexer = new AJavaLex(r, this);
    ts = new TabelaSimbolos();
  }


  static boolean interactive;

  public static void main(String args[]) throws IOException {
    System.out.println("aJava Parser");

    Parser yyparser;
    if ( args.length > 0 ) {
      	// parse a file
      	yyparser = new Parser(new FileReader(args[0]));
    }
    else {
      	// interactive mode
      	System.out.println("[Quit with CTRL-D]");
      	System.out.print("Expression: ");
      	interactive = true;
		yyparser = new Parser(new InputStreamReader(System.in));
    }

    yyparser.yyparse();




    yyparser.ts.print();
    
  }
//#line 600 "Parser.java"
//###############################################################
// method: yylexdebug : check lexer state
//###############################################################
void yylexdebug(int state,int ch)
{
String s=null;
  if (ch < 0) ch=0;
  if (ch <= YYMAXTOKEN) //check index bounds
     s = yyname[ch];    //now get it
  if (s==null)
    s = "illegal-symbol";
  debug("state "+state+", reading "+ch+" ("+s+")");
}





//The following are now global, to aid in error reporting
int yyn;       //next next thing to do
int yym;       //
int yystate;   //current parsing state from state table
String yys;    //current token string


//###############################################################
// method: yyparse : parse input and execute indicated items
//###############################################################
int yyparse()
{
boolean doaction;
  init_stacks();
  yynerrs = 0;
  yyerrflag = 0;
  yychar = -1;          //impossible char forces a read
  yystate=0;            //initial state
  state_push(yystate);  //save it
  val_push(yylval);     //save empty value
  while (true) //until parsing is done, either correctly, or w/error
    {
    doaction=true;
    if (yydebug) debug("loop"); 
    //#### NEXT ACTION (from reduction table)
    for (yyn=yydefred[yystate];yyn==0;yyn=yydefred[yystate])
      {
      if (yydebug) debug("yyn:"+yyn+"  state:"+yystate+"  yychar:"+yychar);
      if (yychar < 0)      //we want a char?
        {
        yychar = yylex();  //get next token
        if (yydebug) debug(" next yychar:"+yychar);
        //#### ERROR CHECK ####
        if (yychar < 0)    //it it didn't work/error
          {
          yychar = 0;      //change it to default string (no -1!)
          if (yydebug)
            yylexdebug(yystate,yychar);
          }
        }//yychar<0
      yyn = yysindex[yystate];  //get amount to shift by (shift index)
      if ((yyn != 0) && (yyn += yychar) >= 0 &&
          yyn <= YYTABLESIZE && yycheck[yyn] == yychar)
        {
        if (yydebug)
          debug("state "+yystate+", shifting to state "+yytable[yyn]);
        //#### NEXT STATE ####
        yystate = yytable[yyn];//we are in a new state
        state_push(yystate);   //save it
        val_push(yylval);      //push our lval as the input for next rule
        yychar = -1;           //since we have 'eaten' a token, say we need another
        if (yyerrflag > 0)     //have we recovered an error?
           --yyerrflag;        //give ourselves credit
        doaction=false;        //but don't process yet
        break;   //quit the yyn=0 loop
        }

    yyn = yyrindex[yystate];  //reduce
    if ((yyn !=0 ) && (yyn += yychar) >= 0 &&
            yyn <= YYTABLESIZE && yycheck[yyn] == yychar)
      {   //we reduced!
      if (yydebug) debug("reduce");
      yyn = yytable[yyn];
      doaction=true; //get ready to execute
      break;         //drop down to actions
      }
    else //ERROR RECOVERY
      {
      if (yyerrflag==0)
        {
        yyerror("syntax error");
        yynerrs++;
        }
      if (yyerrflag < 3) //low error count?
        {
        yyerrflag = 3;
        while (true)   //do until break
          {
          if (stateptr<0)   //check for under & overflow here
            {
            yyerror("stack underflow. aborting...");  //note lower case 's'
            return 1;
            }
          yyn = yysindex[state_peek(0)];
          if ((yyn != 0) && (yyn += YYERRCODE) >= 0 &&
                    yyn <= YYTABLESIZE && yycheck[yyn] == YYERRCODE)
            {
            if (yydebug)
              debug("state "+state_peek(0)+", error recovery shifting to state "+yytable[yyn]+" ");
            yystate = yytable[yyn];
            state_push(yystate);
            val_push(yylval);
            doaction=false;
            break;
            }
          else
            {
            if (yydebug)
              debug("error recovery discarding state "+state_peek(0)+" ");
            if (stateptr<0)   //check for under & overflow here
              {
              yyerror("Stack underflow. aborting...");  //capital 'S'
              return 1;
              }
            state_pop();
            val_pop();
            }
          }
        }
      else            //discard this token
        {
        if (yychar == 0)
          return 1; //yyabort
        if (yydebug)
          {
          yys = null;
          if (yychar <= YYMAXTOKEN) yys = yyname[yychar];
          if (yys == null) yys = "illegal-symbol";
          debug("state "+yystate+", error recovery discards token "+yychar+" ("+yys+")");
          }
        yychar = -1;  //read another
        }
      }//end error recovery
    }//yyn=0 loop
    if (!doaction)   //any reason not to proceed?
      continue;      //skip action
    yym = yylen[yyn];          //get count of terminals on rhs
    if (yydebug)
      debug("state "+yystate+", reducing "+yym+" by rule "+yyn+" ("+yyrule[yyn]+")");
    if (yym>0)                 //if count of rhs not 'nil'
      yyval = val_peek(yym-1); //get current semantic value
    yyval = dup_yyval(yyval); //duplicate yyval if ParserVal is used as semantic value
    switch(yyn)
      {
//########## USER-SUPPLIED ACTIONS ##########
case 4:
//#line 91 "aJava.y"
{ts.tabela.add(new RegistroTS( val_peek(1).sval , "", null ));}
break;
//#line 753 "Parser.java"
//########## END OF USER-SUPPLIED ACTIONS ##########
    }//switch
    //#### Now let's reduce... ####
    if (yydebug) debug("reduce");
    state_drop(yym);             //we just reduced yylen states
    yystate = state_peek(0);     //get new state
    val_drop(yym);               //corresponding value drop
    yym = yylhs[yyn];            //select next TERMINAL(on lhs)
    if (yystate == 0 && yym == 0)//done? 'rest' state and at first TERMINAL
      {
      if (yydebug) debug("After reduction, shifting from state 0 to state "+YYFINAL+"");
      yystate = YYFINAL;         //explicitly say we're done
      state_push(YYFINAL);       //and save it
      val_push(yyval);           //also save the semantic value of parsing
      if (yychar < 0)            //we want another character?
        {
        yychar = yylex();        //get next character
        if (yychar<0) yychar=0;  //clean, if necessary
        if (yydebug)
          yylexdebug(yystate,yychar);
        }
      if (yychar == 0)          //Good exit (if lex returns 0 ;-)
         break;                 //quit the loop--all DONE
      }//if yystate
    else                        //else not done yet
      {                         //get next state and push, for next yydefred[]
      yyn = yygindex[yym];      //find out where to go
      if ((yyn != 0) && (yyn += yystate) >= 0 &&
            yyn <= YYTABLESIZE && yycheck[yyn] == yystate)
        yystate = yytable[yyn]; //get new state
      else
        yystate = yydgoto[yym]; //else go to new defred
      if (yydebug) debug("after reduction, shifting from state "+state_peek(0)+" to state "+yystate+"");
      state_push(yystate);     //going again, so push state & val...
      val_push(yyval);         //for next action
      }
    }//main loop
  return 0;//yyaccept!!
}
//## end of method parse() ######################################



//## run() --- for Thread #######################################
/**
 * A default run method, used for operating this parser
 * object in the background.  It is intended for extending Thread
 * or implementing Runnable.  Turn off with -Jnorun .
 */
public void run()
{
  yyparse();
}
//## end of method run() ########################################



//## Constructors ###############################################
/**
 * Default constructor.  Turn off with -Jnoconstruct .

 */
public Parser()
{
  //nothing to do
}


/**
 * Create a parser, setting the debug to true or false.
 * @param debugMe true for debugging, false for no debug.
 */
public Parser(boolean debugMe)
{
  yydebug=debugMe;
}
//###############################################################



}
//################### END OF CLASS ##############################

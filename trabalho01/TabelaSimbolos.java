import java.util.List;
import java.util.ArrayList;

public class TabelaSimbolos{
	
	List<RegistroTS> tabela;
	
	public TabelaSimbolos(){
		this.tabela = new ArrayList<RegistroTS>();
	}
	
	public void print(){
		for (RegistroTS reg : tabela){
			System.out.printf("%s %s %s\n", reg.id, reg.escopo, "null");		
		} 
	}

}

// Márcio Balotin (15280212-0) e Renata Bueno (14105009-6)

%%

%byaccj

%class AJavaLex
%line

%{
  private Parser yyparser;

  public AJavaLex(java.io.Reader r, Parser yyparser) {
    this(r);
    this.yyparser = yyparser;
  }

 public int getLine() {
     return yyline;
 } 

%}

NUM = [0-9]+ ("." [0-9]+)?

DIGIT=		[0-9]
LETTER=		[a-zA-Z]
WHITESPACE=	[ \t]
LineTerminator = \r|\n|\r\n 

%%



"class"		{ return Parser.CLASS; }

"private"	{ return Parser.PRIVATE; }
"public"	{ return Parser.PUBLIC; }
"int"		{ return Parser.INT; }
"double"	{ return Parser.DOUBLE; }
"boolean"	{ return Parser.BOOLEAN_TYPE; }
"string"	{ return Parser.STRING_TYPE; }
"void"		{ return Parser.VOID; }

"break"		{ return Parser.BREAK; }
"new"		{ return Parser.NEW; }

"true"		{ return Parser.TRUE; }
"false"		{ return Parser.FALSE; }

"if"		{ return Parser.IF; }
"else"		{ return Parser.ELSE; }
"endIf"		{ return Parser.END_IF; }
"for"		{ return Parser.FOR; }
"endFor"	{ return Parser.END_FOR; }
"while"		{ return Parser.WHILE; }
"endWhile"	{ return Parser.END_WHILE; }
"Escreva"	{ return Parser.ESCREVA; }
"Leia"		{ return Parser.LEIA; }
"return"	{ return Parser.RETURN; }

"&&"		{ return Parser.AND; }
"||"		{ return Parser.OR; }
"=="		{ return Parser.EQUALS; }
"!="		{ return Parser.DIFF; }
"+="		{ return Parser.PLUS_EQUAL; }
"-="		{ return Parser.MINUS_EQUAL; }
"++"		{ return Parser.PLUS_PLUS; }
"--"		{ return Parser.MINUS_MINUS; }
"<="		{ return Parser.LOWER_EQUALS; }
">="		{ return Parser.GREATER_EQUALS; }


/* operators */
"+" |
"-" |
"*" |
"/" |
"^" |
"%" |
"(" |
")" |
"{" |
"}" |
":" |
";" |
"," |
"." |
"!" |
">" |
"<" |
"="  { return (int) yycharat(0); }

\"([^\"]|\\.)*\" 	{yyparser.yylval = new ParserVal(yytext());
			return Parser.STRING;}

{LETTER}({LETTER}|{DIGIT}|"\_")* {yyparser.yylval = new ParserVal(yytext()); return Parser.IDENT;}
{NUM}  { yyparser.yylval = new ParserVal(Double.parseDouble(yytext()));
         return Parser.NUM; }

{WHITESPACE}*               { }
{LineTerminator}		{}
/* error fallback */
.          {System.out.println(yyline+1 + ": caracter invalido: "+yytext());}
